#Установка

Клонировать проект 
```git clone https://fresh1000@bitbucket.org/fresh1000/test-task.git```

В корне проекта запустить команду
```composer install```

Создать миграции
```php artisan migrate```

Для доступа из браузера запустить команду
```php artisan serve```

После запуска команды открыть ссылку в браузере 
```http://localhost:8000```

Для просмотра предыдущих результатов поиска открыть ссылку в браузере 
```http://localhost:8000/list-urls```