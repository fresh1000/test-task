<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/parse-url', 'EmailParserController@parseUrl')->name('post-parse');
Route::get('/list-urls', 'EmailParserController@listUrls')->name('list-urls');


Route::get('/', function () {
    return view('welcome');
});
