<?php

namespace App\Services;

use App\Repositories\EmailRepository;

class EmailService
{
    private $emailRepository;

    public function __construct(EmailRepository $emailRepository)
    {
        $this->emailRepository = $emailRepository;
    }

    public function getAllUrlData(): array
    {
        return $this->emailRepository->getAllUrlData();
    }
}