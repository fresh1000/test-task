<?php

namespace App\Services;

class HTMLGenerator
{
    public function generateHtml(array $emails, string &$html = null): string
    {
        foreach ($emails as $key => $elem) {
            if (is_string($key) && is_array($elem)) {
                $html .= '<li><span class="caret">' . $key . '</span><ul class="nested">';
                $this->generateHtml($elem, $html);
                $html .= '</ul></li>';
            } else {
                $html .= '<li>' . $elem . '</li>';
            }
        }
        return $html;
    }

    public function generateHtmlForAllUrls(array $urlsData): string
    {
        $resultHtml = '';
        if (empty($urlsData)) {
            return $resultHtml;
        }
        foreach ($urlsData as $urlEmails) {
            $resultHtml .= $this->generateHtml($urlEmails);
        }

        return $resultHtml;
    }
}