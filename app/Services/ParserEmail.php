<?php

namespace App\Services;

use App\Email;

class ParserEmail
{
    private $patternEmail = '/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/';
    private $patternLink = '@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\\.]*(\\?\\S+)?)?)*)@';
    private $uniqueLinks = [];
    private $emailCounter = 0;

    public function parseEmails(array $options): array
    {
        $depth = (integer) $options['depth'];
        $this->uniqueLinks[] = $options['url'];
        $options['path'][] = $options['url'];
        $options['domain_name'] = parse_url($options['url'], PHP_URL_HOST);

        $emails = $this->extraEmails($options, $depth);

        Email::create(['data' => json_encode($emails)]);

        return $emails;
    }

    private function extraEmails(array $options, int $depth, array &$emailsAll = []): array
    {
        $content = $this->fileGetContentsCurl(end($options['path']));

        $this->uniqueLinks[] = end($options['path']);

        preg_match_all($this->patternEmail, $content, $matches);

        $currentEmails = array_unique($matches[0]);
        $this->emailCounter += count($currentEmails);

        if (!empty($currentEmails)) {
            $this->setArrayValue($options['path'], $emailsAll, $currentEmails);
        }

        if ($depth == 0) {
            return $emailsAll;
        }

        preg_match_all($this->patternLink, $content, $links);

        $extraLinks = $links[0];

        if (empty($extraLinks)) {
            return $emailsAll;
        }

        $linksByDomain = $this->getLinksByDomain($extraLinks, $options['domain_name']);

        if (empty($linksByDomain)) {
            return $emailsAll;
        }

        foreach ($linksByDomain as $link) {
            if ($this->emailCounter < $options['count_emails']) {
                $options['path'][] = $link;
                $this->extraEmails($options, $depth - 1, $emailsAll);
                array_pop($options['path']);
            }
        }

        return $emailsAll;
    }

    private function getLinksByDomain(array $links, string $domainName): array
    {
        $linksByDomain = [];
        foreach ($links as $link) {
            $checkLinkHost = parse_url($link, PHP_URL_HOST);
            if (! empty($checkLinkHost) && $checkLinkHost == $domainName
                && ! in_array($link, $this->uniqueLinks)) {
                $linksByDomain[] = $link;
                $this->uniqueLinks[] = $link;
            }
        }

        return $linksByDomain;
    }

    private function setArrayValue(array $path, array &$array = [], array $value = null)
    {
        $temp = &$array;

        foreach($path as $key) {
            $temp = &$temp[$key];
        }
        $temp = $value;
    }

    private function fileGetContentsCurl(string $url): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
