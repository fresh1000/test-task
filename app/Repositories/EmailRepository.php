<?php

namespace App\Repositories;

use App\Email;

class EmailRepository
{
    public function getAllUrlData(): array
    {
        $emails = Email::all();

        if ($emails->isEmpty()) {
            return [];
        }

        $data = [];
        foreach ($emails as $urlData) {
            $data[] = json_decode($urlData->data, true);
        }

        return $data;
    }
}