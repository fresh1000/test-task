<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ParserEmailData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'count_emails' => 'required|integer|min:1',
            'depth' => 'required|integer|min:0',
            'url' => 'required|string',
        ];
    }
}
