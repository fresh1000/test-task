<?php

namespace App\Http\Controllers;

use App\Http\Requests\ParserEmailData;
use App\Services\EmailService;
use App\Services\HTMLGenerator;
use App\Services\ParserEmail;

class EmailParserController extends Controller
{

    public function parseUrl(ParserEmailData $request, ParserEmail $parserEmail, HTMLGenerator $generator)
    {
        $options = $request->validated();

        $emails = $parserEmail->parseEmails($options);

        $emailsHtml = $generator->generateHtml($emails);

        return view('emails', ['emailsHtml' => $emailsHtml]);
    }

    public function listUrls(EmailService $emailService, HTMLGenerator $generator)
    {
        $emailsData = $emailService->getAllUrlData();

        $htmlData = $generator->generateHtmlForAllUrls($emailsData);

        return view('emails', ['emailsHtml' => $htmlData]);
    }

}
